package com.laosg.spark.base.sql

import java.util.Properties

import org.apache.spark.sql.SparkSession

/**
  * Created by kaimin on 27/5/2019.
  * time : 13:39
  * 创建表的工具类
  */
object SparkSqlUtil {

  /**
    *
    * @param url      连接地址
    * @param username 用户名
    * @param pwd      密码
    * @param tables   注册的表名集合
    * @param spark    spark会话
    */
  def creatTmpAndView(url: String, username: String, pwd: String, tables: List[String], spark: SparkSession): Unit = {
    val property = new Properties()
    property.put("user", username)
    property.put("password", pwd)

    val dfs = for {
      table <- tables
    } yield (table, spark.read.jdbc(url, table, property))

    for {(name, df) <- dfs} df.createOrReplaceTempView(name)
  }
}
