package com.laosg.spark.base.hive

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by kaimin on 15/5/2019.
  * time : 10:26
  */
object HiveOperation {

  def main(args: Array[String]): Unit = {

    val hiveCon = SparkSession.builder().enableHiveSupport().appName("hive_read").getOrCreate()
    val  sqlContext = hiveCon.sqlContext
    sqlContext.table("hive_1.sqoop_test").createOrReplaceTempView("sqoop")
    //读取
    sqlContext.sql("select * from sqoop").show()
    //写入
    sqlContext.sql("insert into sqoop values(3,\"环亚大数据\",18)")
      .collect().foreach(println)
    //读取
    sqlContext.sql("select * from sqoop").show()
    hiveCon.stop()

  }
}
